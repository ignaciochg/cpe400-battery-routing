/**
 * @file Node.cpp
 *
 * @brief Node, Class that behaves like a node
 * 
 * @details Node will interact with other nodes, it uses a custom battery saving routing algorithm
 *
 * @version 1.00 
 *          Ignacio Chamorro(03 Feburary 2018)
 *             Original Implementation
 *                -input data
 *                
 * @note Documentation can be generated using Doxygen
 *
 */

#include "Node.h"

#include <iostream>
#include <stdlib.h>
#include <vector>
#include <algorithm>
using namespace std;

//defined in Node.h
// #define STATUS_NO_CONNECTION 0
// #define STATUS_DEAD          1 //connetion but dead
// #define STATUS_ALIVE         2 

#define IDLE_USAGE 1
#define TRANSMISSION_BATTERY_USAGE 5
#define MIN_BATTERY_THRESHOLD 100



void swap(int a, int b, int *bat, int *indexes){
   int temp = bat[a];
   bat[a] = bat[b];
   bat[b] = temp;

   temp = indexes[a];
   indexes[a] = indexes[b];
   indexes[b] = temp;
}
bool sortByRank( Path &i, Path &j){
   return i.combinedScore < j.combinedScore;//lower the score the better
}




Node::Node(){
   //call init() first to configure 
   //then connect nodesss
   //then call init()
}
void Node::init(int myID, int initialBattery, int *time, Node *otherNodesList,  int *NOfNodes, float NU, float BA /*,int IA*/ ){
   //et all variables to default and set some to the arguments passed in to this method
   id = myID;
   battery = initialBattery;
   numbOfNodes = NOfNodes;
   //int transmissionTimeLeft;// FIX before removing it make sure nobody is using it 
   currentTime = time;//pointer
   if(battery > MIN_BATTERY_THRESHOLD){
      alive = true;
   }
   else{
      alive = false;
   }
   //bool sentBatteryUpdateAfterTransmision;//not needed will use (if(1 and then 0) then we need to send our new batery)
   for(int index=0; index < SEND_BUFFER; index++){
      sendBufferFilled[index] = false;
   }
   for(int index=0; index < SEND_BUFFER; index++){
      receiveBufferFilled[RECEIVE_BUFFER] = false;
   }

   numbOfConections = 0;
   otherNodes =  otherNodesList;// indexed by IDs

   // these will help determine the state of all the network
   //will also help with the number of hops for NU routing (will not have the count to infinity problem)
   for(int i=0; i < (*numbOfNodes); i++){
      for(int j=0; j < (*numbOfNodes); j++){
         connections[i][j] = STATUS_NO_CONNECTION;
         connectionUpdated[i][j] = false;
         connectionLastUpdatedAt[i][j] = -1;
         connetionUpdateBufferUpdated[i][j] = false;
         connectionUpdateBufferTimeStamp[i][j] = -1;
      }
   }

   for(int index=0; index < (*numbOfNodes); index++){
      batteryOfNode[index] = 0;
      batteryUpdated[index] = false;
      batteryLastUpdated[index] = -1 ;//will alow us to calculate bat info, and replace with more updated info, or reject older info
      batteryUpdateBufferUpdated[index] = false;
      batteryUpdateBufferTimeStamp[index] = -1;
   }

   //these not needed since I use a time based sharing algorithm, it will reject it when sending back to whoever gave the info to me
   //so that when we receive info,we can know to call the update bet subroutine, and inform about it to the other nodes on next cycle
   //bool batteryInfoReceived;
   //int batteryInfoReceivedBy;// not needed time used
   //bool batInfoToSend;
   //int sendBatInfoIn;
   //int excludeIDOnBatSend;//in case we need to forward the info, so we dont send back to original sender

   NUPercentage = NU;
   BAPercentage = BA;
   //float IAPercentage;

}

void Node::update(){

   if(battery > 0){
      battery = battery - IDLE_USAGE;
   }
   // if(transmissionTimeLeft > 0){
   //    battery = battery - TRANSMISSION_BATTERY_USAGE ; 
   // }
   if(battery <= MIN_BATTERY_THRESHOLD){
      // if(alive){
         alive = false;
      // }
      // else{
      //    alive = true;
      // }
   }

   // if transmision time left ==1 then == 0
   //    bool sentBatteryUpdateAfterTransmision;


   //////////////////////
   // send updated battery info batery info
   // then move info from battery buffer to main bat 
   //this makes it so that the info is propagated on the next tick
   ////////////////////// same applies to connections
   for(int index=0;index<(*numbOfNodes);index++){
      if(batteryUpdated[index]){
         for(int con=0;con<numbOfConections;con++){
            otherNodes[myConnections[con]].updateBattery(batteryOfNode[index], index, batteryLastUpdated[index]);
         }
         batteryUpdated[index] = false;
      }
      if(batteryUpdateBufferUpdated[index]){
         if(batteryUpdateBufferTimeStamp[index] > batteryLastUpdated[index]){
            batteryLastUpdated[index] = batteryUpdateBufferTimeStamp[index];
            batteryOfNode[index] = batteryUpdateBuffer[index];
            batteryUpdated[index] = true;
            batteryUpdateBufferUpdated[index] = false;
         }
      }
   }


   for(int i=0;i<(*numbOfNodes);i++){
      for(int j=0;j<(*numbOfNodes);j++){
         if(connectionUpdated[i][j]){
            for(int con=0;con<numbOfConections;con++){
               otherNodes[myConnections[con]].updateConnection(i, j, connections[i][j], connectionLastUpdatedAt[i][j]);
            }
            connectionUpdated[i][j] = false;
         }
         if(connetionUpdateBufferUpdated[i][j]){
            if(connectionUpdateBufferTimeStamp[i][j] > connectionLastUpdatedAt[i][j]){
               connectionLastUpdatedAt[i][j] = connectionUpdateBufferTimeStamp[i][j];
               connections[i][j] = connectionUpdateBuffer[i][j];
               connectionUpdated[i][j] = true;
               connectionUpdateBufferTimeStamp[i][j] = false;
            }
         }
      }
   }


  //for every connectionUpdated[][], 
    /*if(connectionLastUpdatedAt[A][B] < *currentTime ){
    do update, and propagate to neighbors(call updateConnection() on adjacent nodes)
  }
  else{//do nothing}*/


   //update my battery info
   //  if we have info to send and info recieved, calculate the newest info for each cell

   //for all spots on send buffer
      //chek if time is +1 of time received then send
      //then mark as empy if successful

         /*   int option = 0;
         int firstOption;
         int secondOption;
         int thirdOption;
         int nextNode = nextHop(destinationID, option++);//will call and determine the best option for next hop
         bool successful = otherNodes[nextNode].receive(destinationID, size);
         while(!successful && option < numbOfConections){//try the next best options if we fail to send it to the best one
            nextNode = nextHop(destinationID, option++);
            successful = otherNodes[nextNode].receive(destinationID, size);
         }*/
      //if timestamp is older then ttl then remove 
      //only sen one at the time


   if(alive){
      for(int index=0; index < RECEIVE_BUFFER; index++){//loop through all the recevied and -- and check if they can be receved
         if(receiveBufferFilled[index] == true){
            if(*currentTime > receiveBufferTimeStamp[index]){
               if(receiveBufferTransmLeft[index] > 0 ){
                  receiveBufferTransmLeft[index]--;
                  battery = battery - TRANSMISSION_BATTERY_USAGE;

                  //cout<<id<<": "<<battery<<endl;
               }
            }
            cout<<id<<": receive buffer "<<index<<" left "<<receiveBufferTransmLeft[index]<<endl;
            if(*currentTime > (receiveBufferTimeStamp[index]+receiveBufferTransmSize[index]) && receiveBufferTransmLeft[index] <= 0){//check if incoming message can be sent(fully received by me)
               bool sendSuccesful = false;
               bool spaceInSendBuffer = false;
               int emptySendIndex = -1;
               int nextHopDestination = -1;

               for(int j=0; j<SEND_BUFFER;j++){//check for space in sending buffer
                  if(sendBufferFilled[j] == false){
                     emptySendIndex = j;
                     spaceInSendBuffer = true;
                     break;
                  }
               }
               if(receiveDestinationBuffer[index] == id){
                  receiveBufferFilled[index] = false;
                  continue;
               }
               if(spaceInSendBuffer){//if done receiving then send
                  int temp = 0;
                  crunchNextHop(receiveDestinationBuffer[index]);
                  while(!sendSuccesful && temp < numbOfConections){
                     nextHopDestination  = nextHop(temp);
                     cout<<id<<": attempting to send to ";
                     cout<< nextHopDestination<<endl;
                     sendSuccesful = send(nextHopDestination,receiveDestinationBuffer[index], receiveBufferTransmSize[index]);//send to other node
                     temp++;
                  }
               }
               if(sendSuccesful){//add to send buffer
                  cout<<id<<": moved from receive buffer to send buffer"<<endl;
                  receiveBufferFilled[index] = false;
                  sendBufferFilled[emptySendIndex] = true;
                  sendDestinationBuffer[emptySendIndex] = receiveDestinationBuffer[index];
                  sendBufferTimeStamp[emptySendIndex] = *currentTime;
                  sendBufferTransmSize[emptySendIndex] = receiveBufferTransmSize[index];
                  nextHopSendDestinationBuffer[emptySendIndex] = nextHopDestination;
                  sendBufferTransmLeft[emptySendIndex] = receiveBufferTransmSize[index];
                  cout<<id<<": setting time left to "<<receiveBufferTransmSize[index]<<endl;
               }
            }

         }
      }
      for(int index=0; index < SEND_BUFFER; index++){//loop through all the send buffer and -- and check if they can be sent
         if(sendBufferFilled[index] == true){
            if(*currentTime > sendBufferTimeStamp[index]){
               if(sendBufferTransmLeft[index] > 0 ){
                  sendBufferTransmLeft[index]--;
                  battery = battery - TRANSMISSION_BATTERY_USAGE;
                  //cout<<id<<": "<<battery<<endl;
               }
            }
            if(sendBufferTransmLeft <= 0){
               sendBufferFilled[index]  = false;
            }
         }

      }
   }


   //send battery info when .. . ?
   //update connection status
   //use modulo to transmit at random time 

}

bool Node::send(int nextHopID, int destinationID, int size){

   //call the receive method of the recepient return its status;
   cout<<id<<": sending() to "<<nextHopID<< " size "<<size<<" dest "<<destinationID<<endl;
   return otherNodes[nextHopID].receive(destinationID, size);
}
bool Node::sendMessage(int toNode, int size){
   bool sendSuccesful = false;
   bool spaceInSendBuffer = false;
   int emptySendIndex = -1;
   int nextHopDestination = -1;

   if(alive){
      for(int j=0; j<SEND_BUFFER;j++){//find empty send buffer
         if(sendBufferFilled[j] == false){
            emptySendIndex = j;
            spaceInSendBuffer = true;
            break;
         }
      }
      if(spaceInSendBuffer){
         int temp = 0;
         crunchNextHop(toNode);
         while(!sendSuccesful && temp < numbOfConections){
            nextHopDestination  = nextHop(temp);
            cout<<id<<": attempting to send to ";
            cout<< nextHopDestination<<endl;
            sendSuccesful = send(nextHopDestination, toNode,size);
            temp++;
         }
      }
      if(sendSuccesful){
         sendBufferFilled[emptySendIndex] = true;
         sendDestinationBuffer[emptySendIndex] = toNode;
         sendBufferTimeStamp[emptySendIndex] = *currentTime;
         sendBufferTransmSize[emptySendIndex] = size;
         nextHopSendDestinationBuffer[emptySendIndex] = nextHopDestination;
         sendBufferTransmLeft[emptySendIndex] = size;
      }      
         
   }
   return sendSuccesful;
}


void Node::updateConnection(int A, int B, int status, int updatedOn){//will be called by external adjaent node
   if( !(A == id || B == id)){// if it refers to not me 
      if(updatedOn > connectionUpdateBufferTimeStamp[A][B]){//update only if current information
         connetionUpdateBufferUpdated[A][B] = true;
         connectionUpdateBuffer[A][B] = status;
         connectionUpdateBufferTimeStamp[A][B] = updatedOn;
         connetionUpdateBufferUpdated[B][A] = true;
         connectionUpdateBuffer[B][A] = status;
         connectionUpdateBufferTimeStamp[B][A] = updatedOn;
      }
   }
   
}
//will use same priciples as update connection
void Node::updateBattery(int value, int batID, int updatedOn){//will be called by external adjaent node 

   if(batID != id){
      if(updatedOn > batteryUpdateBufferTimeStamp[batID]){//update only if current information
         batteryUpdateBufferUpdated[batID] = true;
         batteryUpdateBuffer[batID] = value;
         batteryUpdateBufferTimeStamp[batID] = updatedOn;
      }
   }
}


int Node::calculateBatteryOfNode(int node){//will calculate depending on assumed values, or -1 if dead or not reachable
      int usageSinceLastUpdate = ((*currentTime) - batteryLastUpdated[node]) * IDLE_USAGE ;
      if(usageSinceLastUpdate < 0){
         usageSinceLastUpdate = 0;
      }
      return batteryOfNode[node] - usageSinceLastUpdate;
}

void Node::findRoutes(int s, int d){//depth-first search algorithm
   allPaths.clear();
   Path currentPath;
   currentPath.hops = 0;

   // Mark all the vertices as not visited 
   bool *visited = new bool[*numbOfNodes]; 
   for (int i = 0; i < (*numbOfNodes); i++){
      visited[i] = false; 
   }
  
   bfsHelper(s, d, visited, currentPath); 
   sort(allPaths.begin(), allPaths.end());
}
void Node::bfsHelper(int u, int d, bool visited[], Path &subPath){ 
   // Mark the current node and store it in path[] 
   visited[u] = true;
   subPath.path.push_back(u);
   subPath.hops++; 
  
    // If current vertex is same as destination, then print 
    // current path[] 
   if (u == d){ 
      allPaths.push_back(subPath);
      cout<<id<<": path found: ";
      for (vector<int>::iterator it=subPath.path.begin(); it!=subPath.path.end(); it++){// since vecto<Path> operator= is not implemented we just push the element to make a copy
         if(it != subPath.path.begin()){// if not first
            cout<<", ";
         }
         cout<< *it;
      }
      cout<<endl;
   } 
   else{ // If current vertex is not destination 
      // Recur for all the vertices adjacent to current vertex 
      for(int index=0;index<(*numbOfNodes);index++){
         if(connections[u][index] == STATUS_ALIVE){// 0 no connection, 1 dead(connected but dead), 2 alive
            if(!visited[index]){
               bfsHelper(index,d, visited, subPath);
            }
         }
      }
   }
  
   // Remove current vertex from path[] and mark it as unvisited 
   subPath.hops--; 
   visited[u] = false;
   subPath.path.pop_back();


} 

void Node::routeScoreNU(){//will return a score for the next hop, depending on the ending node
   //since findRoutes() sorts by shortest path then whoever has shortes path is first and so on 
   int index = 0;
   for (vector<Path>::iterator it=allPaths.begin(); it!=allPaths.end(); index++,++it){// since vecto<Path> operator= is not implemented we just push the element to make a copy
      it->NU = ((float)(index+1))/(float)(*numbOfNodes);//1 indexed, and normalize value
      if(it == allPaths.begin()){
         cout<<id<<": NU: 1st: "<< it->path[1]<<endl;
      }
      else if(it == allPaths.begin()+1){
         cout<<id<<": NU: 2nd: "<< it->path[1]<<endl;
      }
      else if(it == allPaths.begin()+3){
         cout<<id<<": NU: 3rd: "<< it->path[1]<<endl;
      }
   }
}
void Node::routeScoreBA(){
   int index = 0;
   int indexes[3];
   int batteries[3];


   for (vector<Path>::iterator it=allPaths.begin(); it!=allPaths.end(); index++,++it){// since vecto<Path> operator= is not implemented we just push the element to make a copy
      it->BA = (float)(*numbOfNodes)/(float)(*numbOfNodes);//assign default score, lower the better the path is, and normalize the value 
   }
   if(allPaths.size() >=3 ){//3
      cout<<id<<": BA: 3 paths found"<<endl;
      batteries[0] = calculateBatteryOfNode(allPaths[0].path[1]);
      batteries[1] = calculateBatteryOfNode(allPaths[1].path[1]);
      batteries[2] = calculateBatteryOfNode(allPaths[2].path[1]);
      indexes[0] = 0;
      indexes[1] = 1;
      indexes[2] = 3;


      if (batteries[0] < batteries[2]){
         swap(0, 2, batteries, indexes);
      }
      if (batteries[0] < batteries[1]){
         swap(0, 1, batteries, indexes);
      }
      if (batteries[1] < batteries[2]){
         swap(1, 2, batteries, indexes);
      }
      allPaths[indexes[0]].BA = 1.0/((float)(*numbOfNodes));//more battery better
      allPaths[indexes[1]].BA = 2.0/((float)(*numbOfNodes));
      allPaths[indexes[2]].BA = 3.0/((float)(*numbOfNodes));
      cout<<id<<": BA: 1st: "<<allPaths[indexes[0]].path[1]<<endl;
      cout<<id<<": BA: 2nd: "<<allPaths[indexes[1]].path[1]<<endl;
      cout<<id<<": BA: 3rd: "<<allPaths[indexes[2]].path[1]<<endl;

   }
   else if(allPaths.size() >= 2 ){//2
      cout<<id<<": BA: 2 paths found"<<endl;
      batteries[0] = calculateBatteryOfNode(allPaths[0].path[1]);
      batteries[1] = calculateBatteryOfNode(allPaths[1].path[1]);
      indexes[0] = 0;
      indexes[1] = 1;
      // cout<<id<<": bat0 "<<calculateBatteryOfNode(allPaths[0].path[1])<<endl;
      // cout<<id<<": bat1 "<<calculateBatteryOfNode(allPaths[1].path[1])<<endl;

      if (batteries[0] < batteries[1]){
         swap(0, 1, batteries, indexes);
      }
      // cout<<id<<": BA: P0 "<< indexes[0]<<endl;
      // cout<<id<<": BA: P1 "<< indexes[1]<<endl;
      cout<<id<<": available paths: "<<allPaths.size()<<endl;
      allPaths[indexes[0]].BA = 1.0/((*numbOfNodes));//more battery better 
      allPaths[indexes[1]].BA = 2.0/((*numbOfNodes));//
      cout<<id<<": BA: 1st: "<<allPaths[indexes[0]].path[1]<<endl;
      cout<<id<<": BA: 2nd: "<<allPaths[indexes[1]].path[1]<<endl;
   }
   else if(allPaths.size() >= 1){//1
      cout<<id<<": BA: 1 path found"<<endl;
      cout<<id<<": BA: 1st: "<<allPaths[0].path[1]<<endl;
      allPaths[0].BA = 1.0/((float)(*numbOfNodes));//more battery better
   }
}
/*void Node::routeScoreIA(int toNode, int nextHopNode){
   return 0.0;
}*/
void Node::routeScoreCombined(){//will determine the combined score based on all three algorithms

   int index = 0;
   for (vector<Path>::iterator it=allPaths.begin(); it!=allPaths.end(); index++,++it){// since vecto<Path> operator= is not implemented we just push the element to make a copy
      it->combinedScore = 0;
      it->combinedScore = ((it->NU)*NUPercentage) + ((it->BA)*BAPercentage);
      cout<<id<<": P"<<index<<" "<<it->path[1] <<" CS: "<< ((it->NU)*NUPercentage) + ((it->BA)*BAPercentage) <<" NUS: "<<((it->NU)*NUPercentage)<<" BAS: "<< ((it->BA)*BAPercentage)<<endl;
   }
}

// bool compareByRank(Path & lhs, Path &rhs){
//    return lhs.combinedScore > rhs.combinedScore;
// }

struct compareByRank {
   bool operator()(const Path& s1, const Path& s2) const {
      return s1.combinedScore < s2.combinedScore;
   }
};


//option index must be smaller than numbOfConections, not checked here
void Node::crunchNextHop(int destination){//will use all algorithms to determine the next hop to take
   findRoutes(id, destination);
   routeScoreNU();//crunch numbers for algorithm 
   routeScoreBA();
   //routeScoreIA();
   routeScoreCombined();
   rankedNextHops.clear();
   int index = 0;
   for (vector<Path>::iterator it=allPaths.begin(); it!=allPaths.end(); it++,index++){// since vecto<Path> operator= is not implemented we just push the element to make a copy
      rankedNextHops.push_back(*it);
      //cout<<"option "<< index <<" path "<<rankedNextHops[index].path[1]<<" CS: "<<it->combinedScore<<endl;
   }
   sort(rankedNextHops.begin(), rankedNextHops.end(), compareByRank() );//[ ]( const Path& lhs, const Path& rhs )
                                                      //   {
                                                        //    return lhs.combinedScore > rhs.combinedScore;
                                                         //});//lower the score the better
   index = 0;
   for (vector<Path>::iterator it=rankedNextHops.begin(); it!=rankedNextHops.end(); it++,index++){// since vecto<Path> operator= is not implemented we just push the element to make a copy
      cout<<id<<": option "<< index <<" path "<<rankedNextHops[index].path[1]<<" CS: "<<it->combinedScore<<endl;
   }

}

int Node::nextHop(int optionIndex){//will use all algorithms to determine the next hop to take
   //find the desired path and
   //extract the next hop (second value in path)
   //cout<<"option "<< optionIndex<<" path "<<rankedNextHops[optionIndex].path[1]<<endl;
   return rankedNextHops[optionIndex].path[1];
}


// bool Node::forward(int senderID, int destinationID, int size, int transmissionID){//if recieving will be cut out by low bat then reject
//   int futureBatteryUsage = (transmissionTimeLeft * IDLE_USAGE)+(transmissionTimeLeft * TRANSMISSION_BATTERY_USAGE);
//   int newBatteryUsage = (size * IDLE_USAGE)+(size * TRANSMISSION_BATTERY_USAGE);
//   int simulatedBatteryRamaining = battery - futureBatteryUsage - newBatteryUsage;
//   if(simulatedBatteryRamaining > MIN_BATTERY_THRESHOLD){
//     transmissionTimeLeft += size;
//     int receiver = nextHop(destinationID);//will call and determine the best option for next hop
//     otherNodes[receiver].send()
//     return true;
//   }
//   return false;
// }




bool Node::receive(int destinationID, int size/*, int transmissionID*/){//if recieving will be cut out by low bat then reject
   //int futureBatteryUsage = (transmissionTimeLeft * IDLE_USAGE)+(transmissionTimeLeft * TRANSMISSION_BATTERY_USAGE);
   

   int newBatteryUsage = (size * IDLE_USAGE)+(size * TRANSMISSION_BATTERY_USAGE);
   int simulatedBatteryRamaining = battery - newBatteryUsage ;//- futureBatteryUsage;
   cout<<id<<": receving size: "<< size<<" Bat usage: "<<newBatteryUsage<<" bat after: "<< simulatedBatteryRamaining<<endl;
   if(simulatedBatteryRamaining > MIN_BATTERY_THRESHOLD){// if we still alive 
      
      //if(id != destinationID){//if not for me then forward, but we cant send it yet, wait for next cycle so wait
         //cout<<"this is not for me, it is for: "<<destinationID<<endl;
         int emptyIndex = -1;
         for(int index = 0; index < RECEIVE_BUFFER; index++){
            if(receiveBufferFilled[index] == false){
               emptyIndex = index;
               break;
            }
         }
         if(emptyIndex == -1){// we cant send since there is no space in the buffer
            return false;
            cout<<id<<": received by " << id <<". DENIED: out of input buffer" <<endl;
         }

         receiveBufferFilled[emptyIndex] = true;
         receiveDestinationBuffer[emptyIndex] = destinationID;
         receiveBufferTimeStamp[emptyIndex] = *currentTime;
         receiveBufferTransmSize[emptyIndex] = size;
         receiveBufferTransmLeft[emptyIndex] = size;
         cout<<id<<": received by "; cout<< id <<endl;

         return true;//since we store on buffer this communication was successful 
      //}
   }
   alive = false;//should update connection array
   cout<<id<<": received by " << id <<". DENIED: will be out of battery if received" <<endl;
   return false;//not delivered successfull since it will run out of battery when sending 
}


void Node::connect(int connectTO){
   myConnections[numbOfConections++] = connectTO;//add connection for me
   otherNodes[connectTO].myConnections[otherNodes[connectTO].numbOfConections++] = id;//add connection for other node
   // connectionUpdated[][] = true;
   // CONNECTION MATRIX NOT NEED TO BE UPDATED HERE, IT WILL BE UPDATED ON THE doneConnecting() method 
   //    connectionUpdated[][] = true;
   // numbOfConections++
   // //update the connection matrix too
}


void Node::doneConnecting(){
   //add my battery info to table and set update flag
   //add connection info to table and set update flag
   // numbOfConections;
   // myConnections[(*numbOfNodes)];
   for(int index=0;index<numbOfConections;index++){
      int other = myConnections[index];
      connections[id][other] = STATUS_ALIVE;// 0 no connection, 1 dead(connected but dead), 2 alive
      connections[other][id] = STATUS_ALIVE;// 0 no connection, 1 dead(connected but dead), 2 alive

      connectionUpdated[id][other]  = true;
      connectionUpdated[other][id]  = true;

      connectionLastUpdatedAt[id][other] = true;
      connectionLastUpdatedAt[other][id] = true;
   }

   updateMyBattery();
}
 void Node::updateMyBattery(){
   batteryOfNode[id] = battery;
   batteryUpdated[id]  = true;//used to know if we need to send information
   batteryLastUpdated[id] = *currentTime;//will alow us to calculate bat info, and replace with more updated info, or reject older info
 
 }


int Node::getID(){
   return id;
}
int Node::getBattery(){
   return battery;
}

void Node::printConnections(){
   cout<<"Con(";
   cout<<numbOfConections;
   cout<<"): ";
   for(int index=0; index<numbOfConections;index++){
      cout<<myConnections[index];
      cout<<" ";
   }
}
// void Node::printConnectionsTable(){

// }
   // bool receiveBufferFilled[RECEIVE_BUFFER];
   // int receiveDestinationBuffer[RECEIVE_BUFFER];
   // int receiveBufferTimeStamp[RECEIVE_BUFFER];
   // int receiveBufferTransmSize[RECEIVE_BUFFER];
   // int receiveBufferTransmLeft[RECEIVE_BUFFER];


void Node::printInBuffer(){
   cout<<id<<": ";
   cout<<"InBI:D:T:S:TL ";
   for(int index=0; index<numbOfConections;index++){
      if(receiveBufferFilled[index]){
         cout<<index;
         cout<< ":";
         cout<< receiveDestinationBuffer[index];
         cout<< ":";
         cout<< receiveBufferTimeStamp[index];
         cout<< ":";
         cout<< receiveBufferTransmSize[index];
         cout<< ":";
         cout<< receiveBufferTransmLeft[index];
         cout<< " ";

      }
   }
}





void Node::printSendBuffer(){
   cout<<id<<": ";
   cout<<"InBI:D:T:S:nhD:TL ";
   for(int index=0; index<numbOfConections;index++){
      if(sendBufferFilled[index]){
         cout<<index;
         cout<< ":";
         cout<< sendDestinationBuffer[index];
         cout<< ":";
         cout<< sendBufferTimeStamp[index];
         cout<< ":";
         cout<< sendBufferTransmSize[index];
         cout<< ":";
         cout<< nextHopSendDestinationBuffer[index];
         cout<< ":";
         cout<< sendBufferTransmLeft[index];
         cout<< " ";

      }
   }
}

void Node::printBatTable(){
   cout<<id<<" bat table: ";
   for(int index=0; index<(*numbOfNodes);index++){
      cout<<index;
      cout<< ":";
      cout<< batteryOfNode[index];
      cout<< ":T";
      cout<< batteryUpdated[index];
      cout<< " "; 
   }
}

