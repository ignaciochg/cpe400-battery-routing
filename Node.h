/**
 * @file Node.h
 *
 * @brief Node, Class that behaves like a node
 * 
 * @details Node will interact with other nodes, it uses a custom battery saving algorithm
 *
 * @version 1.00 
 *          Ignacio Chamorro(03 Feburary 2018)
 *             Original Implementation
 *
 *
 * @note Documentation can be generated using Doxygen
 *
 */

// Precompiler directives 


#ifndef CLASS_NODE_H
#define CLASS_NODE_H

#include <iostream>
#include <vector> 
using namespace std;

#include "Path.h"

#define NUMB_OF_MAX_NODES 20


#define RECEIVE_BUFFER 20 
#define SEND_BUFFER 20 


#define STATUS_NO_CONNECTION 0
#define STATUS_DEAD          1 //connetion but dead
#define STATUS_ALIVE         2 



class Node{
public:


   Node();
   void init(int myID, int initialBattery, int *time, Node *otherNodesList,  int *NOfNodes, float NU, float BA /*,int IA*/ );

   //~Node();

   void update();//will do calculations and other things, need to be called every tick 
   bool sendMessage(int toNode, int size);
   void connect(int connectTO);

   //void printConnections();
   //void printSiimulatedRoute(int to, int size);//should have same arguments as send
   void doneConnecting();

   int getID();
   int getBattery();
   void printConnections();
   void printInBuffer();
   void printSendBuffer();
   void printBatTable();
private:
   int id;
   int battery;
   int transmissionTimeLeft;// FIX before removing it make sure nobody is using it 
   int *currentTime;
   bool alive;
   //bool sentBatteryUpdateAfterTransmision;//not needed will use (if(1 and then 0) then we need to send our new batery)

   bool sendBufferFilled[SEND_BUFFER];
   int sendDestinationBuffer[SEND_BUFFER];
   int sendBufferTimeStamp[SEND_BUFFER];
   int sendBufferTransmSize[SEND_BUFFER];
   int nextHopSendDestinationBuffer[SEND_BUFFER];
   int sendBufferTransmLeft[SEND_BUFFER];

   bool receiveBufferFilled[RECEIVE_BUFFER];
   int receiveDestinationBuffer[RECEIVE_BUFFER];
   int receiveBufferTimeStamp[RECEIVE_BUFFER];
   int receiveBufferTransmSize[RECEIVE_BUFFER];
   int receiveBufferTransmLeft[RECEIVE_BUFFER];

   int numbOfConections;
   int myConnections[NUMB_OF_MAX_NODES];
   Node *otherNodes;//will be array on other nodes, indexed by IDs
   int *numbOfNodes;

   // these will help determine the state of all the network
   //will also help with the number of hops for NU routing (will not have the count to infinity problem)
   int connections[NUMB_OF_MAX_NODES][NUMB_OF_MAX_NODES];// 0 no connection, 1 dead(connected but dead), 2 alive
   bool connectionUpdated[NUMB_OF_MAX_NODES][NUMB_OF_MAX_NODES];
   int connectionLastUpdatedAt[NUMB_OF_MAX_NODES][NUMB_OF_MAX_NODES];
   int connectionUpdateBuffer[NUMB_OF_MAX_NODES][NUMB_OF_MAX_NODES];
   bool connetionUpdateBufferUpdated[NUMB_OF_MAX_NODES][NUMB_OF_MAX_NODES];
   int connectionUpdateBufferTimeStamp[NUMB_OF_MAX_NODES][NUMB_OF_MAX_NODES];
   //int connectionLastUpdatedBy[NUMB_OF_MAX_NODES][NUMB_OF_MAX_NODES];// we dont need this since time will avoid loops of info
   vector<Path> allPaths;//will store all paths, will be sorted, must call crunchNextHop() before using
   void findRoutes(int s, int d);//depth-first search algorithm, calculates all paths and sorts them by shortest, used once per routing
   vector<Path> rankedNextHops;// paths ranked based on all three algorithms 
   void bfsHelper(int u, int d, bool visited[], Path &subPath); 

   //for incoming information
   void updateConnection(int A, int B, int status, int updatedOn);
   void updateBattery(int value, int batID, int updatedOn);//uses same update prisiples as the update connection

   int batteryOfNode[NUMB_OF_MAX_NODES];//bat of node at given time, if smaller than 0 then we never had a value for it
   bool batteryUpdated[NUMB_OF_MAX_NODES];//used to know if we need to send information
   int batteryLastUpdated[NUMB_OF_MAX_NODES];//will alow us to calculate bat info, and replace with more updated info, or reject older info
   int calculateBatteryOfNode(int node);//will calculate depending on assumed values, or -1 if dead
   int batteryUpdateBuffer[NUMB_OF_MAX_NODES];//battery value, incoming buffer (one spot per device)
   bool batteryUpdateBufferUpdated[NUMB_OF_MAX_NODES];//used to know if we recevied information
   int batteryUpdateBufferTimeStamp[NUMB_OF_MAX_NODES];// to make sure we have the most updated information
   //int batteryInfoReceivedBy[NUMB_OF_MAX_NODES];
   void updateMyBattery();
   //so that when we receive info,we can know to call the update bet subroutine, and inform about it to the other nodes on next cycle
   bool batteryInfoReceived;
   bool batInfoToSend;
   int sendBatInfoIn;
   int excludeIDOnBatSend;//in case we need to forward the info, so we dont send back to original sender

   float NUPercentage;
   float BAPercentage;
   //float IAPercentage;

   void routeScoreNU();//will return a score for the next hop, depending on the ending node
   void routeScoreBA();//same
   //float routeScoreIA();//same
   void routeScoreCombined();//will determine the combined score based on all three algorithms
   int nextHop(int optionIndex);//will use all algorithms to determine the next hop to take, crunchNextHopTo() must be called before using
   void crunchNextHop(int destination);
   
   //bool forward(int senderID, int destinationID, int size, int transmissionID);//if recieving will be cut out by low bat then reject
   bool receive(int destinationID, int size/*, int transmissionID*/);//if recieving will be cut out by low bat then reject

   bool send(int nextHopID, int destinationID, int size);//will only be called by the initial sending party, not by intermediate nodes

};


// Terminating precompiler directives  

#endif // ifndef CLASS_NODE_H
