#ifndef PATH_H
#define PATH_H


#include <vector> 
using namespace std;

class Path;
//bool sortByRank(const Path &i, const Path &j);

class Path {
   public:
      Path();
     int hops;
     float combinedScore;
     float NU ;
     float BA;
     //float IA = 0;
     vector<int> path;
     bool operator<(const Path& other) const;
};


#endif // PATH_H
