COMPILER = g++
FLAGS = -Wall -g -std=c++11

sim: simulation.o Node.o Path.o
	$(COMPILER) $(FLAGS) -o sim simulation.o Node.o Path.o
simulation.o: simulation.cpp
	$(COMPILER) $(FLAGS) -c simulation.cpp
Node.o: Node.cpp Node.h
	$(COMPILER) $(FLAGS) -c Node.cpp
Path.o: Path.h Path.cpp 
#	$(COMPILER) $(FLAGS) -c Path.cpp
clean:
	rm -f *.o sim
