/**
* @file simulation.cpp
*
* @brief Battery optimized Routing CPE400
* 
* @details OS Simulation 
*
* @version 1.00 
*          Ignacio Chamorro(03 Feburary 2018)
*             Original Implementation
*                -Configuration Read
*                -MetaData Read
*                -Time Calculation
*
* @note Documentation can be generated using Doxygen
*/

//#include <pthread.h>
#include <iostream> 
//#include <stdlib.h>  //srtof() convert string to float
#include <fstream>
//#include <iomanip>   // using setprecision(n), to make cout<<version look good
#include <string> 
#include <vector>
#include <cstdlib>

#include <unistd.h> //usleep

using namespace std;

#include "Node.h"

//Algorithms split, deprecated will be set by each test case
//#define NET_USAGE  0.50
//#define BAT_AVOID  0.50
//#define IMP_AVOID  0.00 //not implemented
//              +
//             --------------
//                 100%  

#define BATTERY 5000

void init0();
void init1();
void init2();
void main0();
void main1();
void main2();
void tick(int ticks = 1);
void printStatus();
void printHelp();

int *myTime = new int(0) ;
bool EXIT = false;
Node *myNodes;

int *numbOfNodes = new int(0);
float NU = 1;
float BA = 0;
float IA = 0;
//   +
//-----------
//        1 = 100%



int main(int argc, char const *argv[])
{

   if(argc <=1 || argc >=3){
      printHelp();
      return 1;
   }
   int simulation = atoi(argv[1]); 
   if(simulation < 0 || simulation >1 ){
      cout<<"simulation "<<simulation<<" not a valid simulation"<<endl<<endl; 
      printHelp();
      return 1;
   }


   switch(simulation) {
      case 0 : main0(); break;
      case 1 : main1(); break;
   }

   cout<<"bye"<<endl;
   return 0;
}


void tick(int ticks){

   for(int t=0; t<ticks ;t++){
      (*myTime)++;
      for(int index=0; index < *numbOfNodes;index++){
         myNodes[index].update();
      }
      cout<<"T";
      cout<<(*myTime)<<endl;
   }
}


void printStatus(){
   cout<<"#### T:";
   cout<< (*myTime);
   cout<< " #############"<<endl;

   for(int index=0; index < *numbOfNodes;index++){
      cout<< myNodes[index].getID();
      cout<<": B";
      cout<< myNodes[index].getBattery();
      cout<<" ";
      myNodes[index].printConnections();
      cout<<endl;
   }
   cout<<"######################"<<endl;

}


void init0(){
   //configure connections
   *numbOfNodes =  5;
   NU = 0.51;
   BA = 0.49;
   //IA = 0;
   myNodes = new Node[*numbOfNodes];
   int bat[] = {
                  1000,1000,800,1000,1000
               };
   for(int index=0; index<*numbOfNodes; index++){
      myNodes[index].init(index, bat[index], myTime, myNodes, numbOfNodes, NU, BA );
   }
   
   myNodes[0].connect(1);
   myNodes[0].connect(2);
   myNodes[1].connect(3);
   myNodes[2].connect(4);
   myNodes[3].connect(4);

   for(int index=0; index<*numbOfNodes; index++){
      myNodes[index].doneConnecting();
   }

   EXIT = false;
}
void main0(){
   init0();

   //cout << "test"<<endl;
    
   //while(!EXIT){
      //get input 
   //}
   printStatus();
   tick(20);
   printStatus();
   //myNodes[0].printBatTable();
   //cout<<endl;
   myNodes[0].sendMessage(4, 10);//to 4 size 10
   tick();
   myNodes[2].printInBuffer();
   cout<<endl;
   tick(10);
   printStatus();
   tick();

   //printStatus();
   //printStatus();
}

void init1(){
   //configure connections
   *numbOfNodes =  5;
   NU = 0.0;
   BA = 1.0;
   //IA = 0;
   myNodes = new Node[*numbOfNodes];
   int bat[] = {
                  1000,1000,800,1000,1000
               };
   for(int index=0; index<*numbOfNodes; index++){
      myNodes[index].init(index, bat[index], myTime, myNodes, numbOfNodes, NU, BA );
   }
   
   myNodes[0].connect(1);
   myNodes[0].connect(2);
   myNodes[1].connect(3);
   myNodes[2].connect(4);
   myNodes[3].connect(4);

   for(int index=0; index<*numbOfNodes; index++){
      myNodes[index].doneConnecting();
   }

   EXIT = false;
}
void main1(){
   init1();

   cout << "test"<<endl;
    
   //while(!EXIT){
      //get input 
   //}
   printStatus();
   tick(20);
   printStatus();
   //myNodes[0].printBatTable();
   //cout<<endl;
   myNodes[0].sendMessage(4, 10);//to 4 size 10
   tick();
   myNodes[2].printInBuffer();
   cout<<endl;
   tick(11);
   printStatus();
   tick(11);
   printStatus();
   tick(11);
   //printStatus();
   //printStatus();
}



void printHelp(){
   cout<<"Simulation using Node library"<<endl;
   cout<<"     By Ignacio Chamorro"<<endl;
   cout<<""<<endl;
   cout<<"usage: "<<endl;
   cout<<"  ./sim <simNumb>"<<endl;
   cout<<""<<endl;
   cout<<"simulations:"<<endl;
   cout<<"  0         Simulation that shows the network battery saving algorthm."<<endl;
   cout<<"  1         Simulation that shows the battery avoidance algorithm."<<endl;


}